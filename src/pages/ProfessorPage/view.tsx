import * as React from "react";
import Card from "../../components/Card";
import ProfessorInfo from "../../types/ProfessorInfo";
import styles from "./professorPage.module.css";

interface IProfessorPageView {
  professorInfo: ProfessorInfo | undefined
}

const ProfessorPageView = (props: IProfessorPageView) => {
  const {professorInfo} = props;

  if (professorInfo)
    return(
      <div>
        <header className={styles.main_header}>
          <div className={styles.container}>
            <div className={styles.row}>
              <img src={"logo.png"} alt="logo" className={styles.logo}/>
              <h1 className={styles.titulo}>Vitrine da Ciência</h1>
            </div>          
          </div>
        </header> 
        <div className={styles.profileInfoContainer}>
          <div className={styles.profileInfo}>
            <div className={styles.profilePhoto}>
              <div
                className={styles.profilePhotoImg}
                style={{
                  backgroundImage: `${professorInfo.profileImg}`
                }}
              >

              </div>
            </div>
            <div className={styles.profileText}>
                <p className={styles.professorName}>
                  {professorInfo.name}
                </p>
                <p className={styles.professorTitle}>
                  {professorInfo.title}
                </p>
                <div className={styles.professorFields}>
                  {professorInfo.fields.map(item => (
                    <p>
                      {item}
                    </p>
                  ))}
                </div>
            </div>
          </div>
        </div>
        <div className={styles.researchesContainer}>
          <h1 className={styles.researchesContainerTitle}>
            PESQUISAS
          </h1>
          <div className={styles.researchesCards}>
            {professorInfo.researches.map(item => (
              <Card 
                img={item.img}
                title={item.title}
                description={item.description}
              /> 
            ))}
          </div>
        </div>
      </div>
    )

  return(
    <div>
      Loading...
    </div>
  )
}

export default ProfessorPageView;