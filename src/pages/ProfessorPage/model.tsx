import ProfessorInfo from "../../types/ProfessorInfo"

const loadProfessorInfo = async (id: string): Promise<ProfessorInfo> => {
  //Acesso ao firebase

  return (
    {
      profileImg: "https://lh3.googleusercontent.com/a-/AOh14Gjgv7wlS-FawwVUCGxVO1MMHkfiTqFl5DCLV2PXQg=s240-p-k-rw-no",
      name: "Lindon Jonathan Sanley dos S P Monroe",
      title: "Título",
      fields: ["Programação dinâmica","Criptografia","Machine Learning"],
      researches: [
        {
          img: "https://images.unsplash.com/photo-1603126857599-f6e157fa2fe6?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1950&q=80",
          title: "Projeto X",
          description: "Descrição blablabla"
        },
        {
          img: "https://images.unsplash.com/photo-1603126857599-f6e157fa2fe6?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1950&q=80",
          title: "Projeto X",
          description: "Descrição blablabla"
        },
        {
          img: "https://images.unsplash.com/photo-1603126857599-f6e157fa2fe6?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1950&q=80",
          title: "Projeto X",
          description: "Descrição blablabla"
        }
      ]
    }
  )
}

export {loadProfessorInfo}