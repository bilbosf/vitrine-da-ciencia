import * as React from "react";
import ProfessorInfo from "../../types/ProfessorInfo";
import { loadProfessorInfo } from "./model";

const ProfessorPageController = (props: any) => {
  const [professorInfo, setProfessorInfo] = React.useState<ProfessorInfo>();

  React.useEffect(() => {
    initPage();
  },[])

  const initPage = async () => {
    const response = await loadProfessorInfo(props.id);

    setProfessorInfo(response);
  }

  return React.cloneElement(props.children, {
    professorInfo: professorInfo
  });
}

export default ProfessorPageController;