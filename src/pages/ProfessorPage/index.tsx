import * as React from "react";
import ProfessorPageController from "./controller";
import ProfessorPageView from "./view";

const ProfessorPage = () => {
  return(
    <ProfessorPageController>
      <ProfessorPageView 
        professorInfo={undefined}
      />
    </ProfessorPageController>
  )
}

export default ProfessorPage;