import * as React from "react";
import DashboardProfController from "./controller";
import DashboardProfView from "./view";

const DashboardProf = () => {
  return(
    <DashboardProfController>
      <DashboardProfView/>
    </DashboardProfController>
  )
}

export default DashboardProf;