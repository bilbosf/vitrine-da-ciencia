import * as React from "react";
import Card from "../../components/Card";
import styles from "./dashboardProf.module.css";
import {
  Button,
  TextField,
  IconButton,
} from '@material-ui/core';

import DeleteOutlinedIcon from '@material-ui/icons/DeleteOutlined';

const DashboardProfView = (props : any) => {
  return(
    <div>
      <header className={styles.main_header}>
        <div className={styles.container}>
          <div className={styles.row}>
            <img src={"logo.png"} alt="logo" className={styles.logo}/>
            <h1 className={styles.titulo}>Vitrine da Ciência</h1>
          </div>          
        </div>
      </header> 
      <div className={styles.profileInfoContainer}>
        <div className={styles.profileInfo}>
          <div className={styles.profilePhoto}>
            <div
              className={styles.profilePhotoImg}
              style={{
                backgroundImage: "https://lh3.googleusercontent.com/a-/AOh14Gjgv7wlS-FawwVUCGxVO1MMHkfiTqFl5DCLV2PXQg=s240-p-k-rw-no"
              }}
            >

            </div>
          </div>
          <div className={styles.profileText}>
              <div className={styles.professorName}>
                <TextField
                  id="outlined-helperText"
                  label="Nome do Professor"
                  defaultValue="Lindon Jonathan Sanley dos S P Monroe"
                />
              </div>
              <div className={styles.professorName}>
                <TextField
                  id="outlined-helperText"
                  label="Título"
                  defaultValue="Professor Doutor"
                /> 
              </div>
              <div className={styles.professorFieldsForm}>
                  <TextField
                    id="outlined-helperText"
                    label="Áreas de Pesquisa"
                    helperText="Adicione uma área de pesquisa "
                  />
              </div>
              <div className={styles.professorFields}>
                <div>
                  <p>
                    Programação dinâmica
                    <IconButton aria-label="delete" size="small">
                      <DeleteOutlinedIcon fontSize="inherit" /> 
                    </IconButton> 
                  </p>
                </div>
                <div>
                  <p>
                    Criptografia
                    <IconButton aria-label="delete" size="small">
                      <DeleteOutlinedIcon fontSize="inherit" /> 
                    </IconButton> 
                  </p>
                </div>
                <div>
                  <p>
                    Machine Learning
                    <IconButton aria-label="delete" size="small">
                      <DeleteOutlinedIcon fontSize="inherit" /> 
                    </IconButton> 
                  </p>
                </div>
              </div>
          </div>
        </div>
      </div>
      <div className={styles.researchesContainer}>
        <h1 className={styles.researchesContainerTitle}>
          PESQUISAS
        </h1>
        <Button variant="outlined" id={styles.includeResearchButton}>
         Adicionar uma pesquisa
        </Button>
        <div className={styles.researchesCards}>
          <Card 
            img="https://images.unsplash.com/photo-1603126857599-f6e157fa2fe6?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1950&q=80"
            title={"Projeto X"}
            description={"Descrição blablabla"}
          /> 
          <Card 
            img="https://images.unsplash.com/photo-1603126857599-f6e157fa2fe6?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1950&q=80"
            title={"Projeto X"}
            description={"Descrição blablabla"}
          /> 
          <Card 
            img="https://images.unsplash.com/photo-1603126857599-f6e157fa2fe6?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1950&q=80"
            title={"Projeto X"}
            description={"Descrição blablabla"}
          /> 
          <Card 
            img="https://images.unsplash.com/photo-1603126857599-f6e157fa2fe6?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1950&q=80"
            title={"Projeto X"}
            description={"Descrição blablabla"}
          />
          <Card 
            img="https://images.unsplash.com/photo-1603126857599-f6e157fa2fe6?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1950&q=80"
            title={"Projeto X"}
            description={"Descrição blablabla"}
          />
          <Card 
            img="https://images.unsplash.com/photo-1603126857599-f6e157fa2fe6?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1950&q=80"
            title={"Projeto X"}
            description={"Descrição blablabla"}
          />
        </div>
      </div>
    </div>
  )
}

export default DashboardProfView;