import React, {useEffect, useState} from 'react';
import styles from './home.module.css';


import Card from '../../components/Card';
import {
  Button, 
  Menu,
  MenuItem,
  Link,
  Typography, 
  Breadcrumbs
} from '@material-ui/core';
import firebase from '../../services/firebase';
import FormValues from '../../types/FormValues';

import SearchBar from '../../components/SearchBar';
import CardsContainer from '../../components/CardsContainer';
import LoginModal from '../../components/LoginModal';

function handleClick(event: React.MouseEvent<HTMLAnchorElement, MouseEvent>) {
  event.preventDefault();
  console.info('You clicked a breadcrumb.');
}

const Home = () => {
  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);
  const [cardsInfo, setCardsInfo] = useState<FormValues[]>([]);

  const [showModal, setShowModal] = useState(false); //let


  //Renderização inicial da página vai chamar 'loadItems' para carregar as infos do banco de dados
  useEffect(() => {
    loadItems();
  },[]);

  const preventDefault = (event: React.SyntheticEvent) => event.preventDefault();

  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  //Função que recebe os dados do banco de dados
  const loadItems = () => {
    firebase.firestore().collection("forms").get()
      .then(collection => {
        const docs = collection.docs;
        const cardsInfoAux: any = docs.map(doc => doc.data());
        setCardsInfo([...cardsInfoAux]);

        console.log(cardsInfoAux);
      })
      .catch(error => alert(error));
  }

  return (
    <div>
      <header className={styles.main_header}>
        <div className={styles.container}>
          <div className={styles.row}>
            <img src={"logo.png"} alt="logo" className={styles.logo}/>
            <h1 className={styles.titulo}>Vitrine da Ciência</h1>
          </div>
          
          <Button variant="contained" onClick={() => setShowModal(true)}>Login</Button>
          {showModal &&
            <LoginModal
              handleClose={() => setShowModal(false)}       
            />
          }

          <SearchBar className={styles.searchBar} setCardsInfo={setCardsInfo}/>          
        </div>
      </header> 
      <CardsContainer 
        cardsInfo={cardsInfo}
      />
    </div>
  )
}

export default Home;