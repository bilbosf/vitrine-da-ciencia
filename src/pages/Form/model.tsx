import firebase from "../../services/firebase";
import FormValues from "../../types/FormValues";

const submitForm = async (data: any) => {

  
  if(data.titulo.length < 1 || data.titulo.length > 100){
    throw new Error('"Título de pesquisa" não preenchido ou não válido');
  }
  if(data.institutos.length < 1 || data.institutos.length > 500){
    throw new Error('"Institutos envolvidos" não preenchido ou não válido');
  }
  if(data.descricao.length < 1 || data.descricao.length > 5000){
    throw new Error('"Descrição da pesquisa" não preenchido ou não válido');
  }
  if(data.contato.length < 1 || data.contato.length > 300){
    throw new Error('"Como interessados entrar em contato" não preenchido ou não válido');
  }

  let response = await firebase.firestore().collection("forms").doc().set({
    ...data
  })

  return response;
}

export {submitForm};