import * as React from "react";
import { useForm } from "react-hook-form";
import styles from "./form.module.css";
import FormValues from "../../types/FormValues";

const FormView = (props: any) => {
  const {submit} = props;

  const {
    register,
    handleSubmit,
    formState: { errors }
  } = useForm<FormValues>();

  return (
    <div className={styles.fundo_form}>

    <form
      onSubmit={handleSubmit((data) => {
        console.log(data);
        submit(data)
      })}
      className={styles.formulario}
    >
      <div>
        <h1 className={styles.titulo}>
          Envio de pesquisa
        </h1>
      </div>
      <div>
        <h4>
          Dados da pesquisa
        </h4>
      </div>
      <label htmlFor="titulo" className={styles.form_label}>Título da pesquisa:</label>
      <input
        {...register("titulo", { required: "Dado obrigatório." })}
        id="titulo"
        className={styles.form_input}
      />
      {errors.titulo && <p className={styles.paragrafo}>{errors.titulo.message}</p>}

      <label htmlFor="institutos" className={styles.form_label}>Institutos envolvidos:</label>
      <input
        {...register("institutos", { required: "Dado obrigatório." })}
        id="institutos"
        className={styles.form_input}
      />
      {errors.institutos && <p className={styles.paragrafo}>{errors.institutos.message}</p>}

      <label htmlFor="descricao" className={styles.form_label}>Descrição da pesquisa:</label>
      <textarea
        {...register("descricao", { required: "Dado obrigatório." })}
        id="descricao"
        className={styles.form_textarea}
      />
      {errors.descricao&& <p className={styles.paragrafo}>{errors.descricao.message}</p>}

      <label htmlFor="contato" className={styles.form_label}>Como os interessados devem entrar em contato?</label>
      <input
        {...register("contato", { required: "Dado obrigatório." })}
        id="contato"
        className={styles.form_input}
      />
      {errors.contato && <p className={styles.paragrafo}>{errors.contato.message}</p>}

      <input type="submit" className={styles.form_input} id={styles.btn_submit} />
    </form>

    </div>
  );
}

export default FormView;