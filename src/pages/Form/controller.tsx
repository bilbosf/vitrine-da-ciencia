import * as React from "react";
import FormValues from "../../types/FormValues";
import {submitForm} from "./model";

const FormController = (props: any) => {
  const handleSubmit = async (data:FormValues) => {
    try{
      const response = await submitForm(data);
      console.log("Sucesso");
      console.log(response);
    }
    catch(err){
      alert(err);
    }
    //Analise da resposta pra retornar sucesso ou falha
  }

  return React.cloneElement(props.children, {
    submit: handleSubmit
  });
}

export default FormController