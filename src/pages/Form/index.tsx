import * as React from "react";
import FormController from "./controller";
import FormView from "./view";

const Form = () => {
  return(
    <FormController>
      <FormView />
    </FormController>
  )
}

export default Form;

