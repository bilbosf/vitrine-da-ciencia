import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import Form from './pages/Form';
import Home from './pages/Home';
import DashboardProf from './pages/DashboardProf'
import ProfessorPage from './pages/ProfessorPage';

const Routes = () => (
  <BrowserRouter>
    <Switch>
      <Route exact path="/" component={Home} />
      <Route path="/form" component={Form} />
      <Route path="/dashboard" component={DashboardProf} />
      <Route path="/professorPage" component={ProfessorPage} />
    </Switch>
  </BrowserRouter>
);

export default Routes;