type FormValues = {
  nomeCompleto: string;
  relacao: string;
  outra: string;
  email: string;
  titulo: string;
  institutos: string;
  descricao: string;
  objetivo: string;
  contato: string;
  [key: string]: string;
};

export default FormValues;