type ProfessorInfo = {
  profileImg: string,
  name: string,
  title: string,
  fields: string[],
  researches: {
    img: string,
    title: string,
    description: string
  }[]
}

export default ProfessorInfo;