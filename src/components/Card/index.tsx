import React, { useState } from  'react';
import ResearchModal from '../ResearchModal';
import styles from "./card.module.css";

const Card = (props: any) => {
  //Variável de estado que define se o modal da pesquisa está sendo apresentado ou não
  const [showModal, setShowModal] = useState(false);

  return(
    <>
      <div className={styles.card}>
        <div className={styles.card__body}>
          <img src= {props.img}  className={styles.card__image}/>
          <h2 className={styles.card__title}>{props.title}</h2>
          <p className={styles.card__description}>{props.description}</p>
        </div>
        <button className={styles.card__btn} onClick={() => setShowModal(true)}>
          Veja mais
        </button>
      </div>
      {showModal &&
        <ResearchModal
          image="https://images.unsplash.com/photo-1603126857599-f6e157fa2fe6?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1950&q=80"
          title="Biologia Estrutural e Cristalografia"
          author="Professor X"
          handleClose={() => setShowModal(false)}       
        />
      }
    </>
  )
}

export default Card;