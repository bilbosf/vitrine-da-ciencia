import React, { useState } from 'react';
import styles from './modal.module.css';
import Dialog from '@material-ui/core/Dialog';
import Button from '@material-ui/core/Button';

interface IResearchModal {
  image: string,
  title: string,
  author: string,
  handleClose: () => void
}

const ResearchModal = (props: IResearchModal) => {
  const {image, title, author, handleClose} = props;

  return (
    <div style={{position: "absolute"}}>
      <Dialog classes={{paper:styles.modalBoxPaper}} PaperProps = {{style: {borderRadius: 40}}} onClose={handleClose} open>
          <div className={styles.modalTitle}>
              <img src={image} alt={title} style={{width: '40%'}} />
              <div>
                  {title} <br />
                  {author}
              </div>
          </div>

          <div className={styles.modalContent}>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent in turpis sem. In mi neque, rutrum ac semper interdum, venenatis ut nisl. Aenean ornare, odio nec volutpat rutrum, justo lacus faucibus sem, at tincidunt ante nibh sed purus. Aliquam urna nibh, scelerisque vel ligula posuere, commodo faucibus est. In hac habitasse platea dictumst. Integer at egestas arcu.
          </div>

          <div className={styles.modalButton}>
              <Button variant="outlined" color="primary" onClick={handleClose}>
                  Salvar como PDF
              </Button>
          </div>

      </Dialog>
    </div>
  );
};

export default ResearchModal;