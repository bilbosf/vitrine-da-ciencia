import React, { useState } from 'react';
import styles from './login.module.css';
import Dialog from '@material-ui/core/Dialog';
import Button from '@material-ui/core/Button';
import { Grid, Paper, TextField, Typography, Link} from '@material-ui/core';
import {login} from "./model";



import { useHistory } from 'react-router';

interface ILoginModal {
    handleClose: () => void;
}

const LoginModal =  (props: ILoginModal) => {

    const {handleClose} = props;

    const [authenticating, setAuthenticating] = useState<boolean>(false);
    // const [error, setError] = useState<string>('');

    const history = useHistory();

    const email = React.useRef("");
    const password = React.useRef("");

    const handleLogin = async (email: string, password: string) => {
        try {
            await login(email, password);
        } catch (error: any) {
            alert(error);
        }

        handleClose();
    }

    const paperStyle={padding: 20, height: 419, width: 388, margin: "20px auto"}
    const textFieldStyle = {margin: "8px 0"}
    const button1Style = {margin: "8px 0", backgroundColor: '#413692'}
    const button2Style = {backgroundColor: '#E8DEF8'}
    const titleStyle = {margin: "0 0 24px"}
    

    return (
    
        <div style={{position: "absolute"}}>
            <Dialog classes={{paper:styles.modalBoxPaper}} PaperProps = {{style: {borderRadius: 8}}} onClose={handleClose} open>
                <Grid>
                    <Paper style={paperStyle}>
                        <div className={styles.modalTitle} style= {titleStyle}>
                            Acesse sua conta <br/>
                            de professor
                        </div> 
                        <div>
                            <TextField id="outlined-basic" label="E-mail" variant="outlined" style={textFieldStyle} fullWidth required onChange={(event) => {email.current = event.target.value}}/>
                            <TextField id="outlined-password-input" label="Senha" variant="outlined" type="password" autoComplete="current-password" fullWidth required onChange={(event) => {password.current = event.target.value}}/>
                        </div>

                        <Typography style={textFieldStyle}>
                            <Link href="#">
                                Esqueci minha senha
                            </Link>
                        </Typography>

                        <Button onClick={() => handleLogin(email.current, password.current)} type="submit" color ="primary" style={button1Style} fullWidth variant="contained" disabled={authenticating}>Entrar</Button>
                        <Button type="submit" style={button2Style} fullWidth variant="contained">Cadastre-se</Button>

                      </Paper>

                </Grid> 
                
                
            </Dialog>
        </div>
    );
};

export default LoginModal;