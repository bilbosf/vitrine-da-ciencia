import React, { useState } from 'react';
import { auth, signInWithEmailAndPassword } from '../../services/firebase';
const login = async (email: string, password: string) => {
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    
    if (email.match(re)) {
        try {
            await auth.signInWithEmailAndPassword(email, password) 
            .then(user => {
                console.log(user.user?.email);
                // abre o dashboard do professor
            })

            return "Efetuar login";

        } catch (err: any) {
            if (err.code === "auth/user-not-found") {
                throw new Error("O email não está cadastrado!");
            }
            if (err.code === "auth/wrong-password") {
                throw new Error("Senha incorreta!");
            }
        }

    } else {
        throw new Error("Email inválido!");
    }
    
};
export {
    login
}
