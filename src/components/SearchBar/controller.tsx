import * as React from "react";
import {submitForm} from "./model";

const SearchBarController = (props: any) => {
  const handleSubmit = async (data: string) => {
    try{
      const result = await submitForm(data);
      console.log(result);
      props.setCardsInfo(result);
    }
    catch(err: any){
      alert(err)
    }
  }

  return React.cloneElement(props.children, {
    submit: handleSubmit
  });
}

export default SearchBarController