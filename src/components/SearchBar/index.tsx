import React, { useState } from 'react';
import SearchBarController from './controller';
import SearchBarView from './view';

const SearchBar = (props: any) => {
    return(
        <SearchBarController setCardsInfo={props.setCardsInfo}>
            <SearchBarView/>
        </SearchBarController>
    );
};

export default SearchBar;