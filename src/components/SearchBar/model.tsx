import firebase from "../../services/firebase";
import FormValues from '../../types/FormValues';

const searchInField = (data_arr: string[], field: keyof FormValues, cards: FormValues[], result: FormValues[]) => {

  for (var card of cards) {
    if(field in card){
      const value_norm = card[field].normalize("NFD").replace(/\p{Diacritic}/gu, "").toLowerCase();

      for (var word of data_arr){
        if(value_norm.includes(word)){
          result.push(card);
          cards.splice(cards.indexOf(card), 1); // Remove o item do array
        }
      }
    }
  }
}

const submitForm = async (data: string) => {
  
  if(data.length < 1 || data.length > 100){
    throw new Error("Pesquisa inválida!");
  }

  var result: FormValues[] = []
  const data_norm = data.normalize("NFD").replace(/\p{Diacritic}/gu, "").toLowerCase();
  const data_arr = data_norm.split(" ");

  await firebase.firestore().collection("forms").get()
      .then(collection => {
        const docs = collection.docs;
        const cardsInfoAux: any = docs.map(doc => doc.data());
        const cards: FormValues[] = [...cardsInfoAux];

        searchInField(data_arr, 'titulo', cards, result);
        searchInField(data_arr, 'nomeCompleto', cards, result);
        searchInField(data_arr, 'descricao', cards, result);
        searchInField(data_arr, 'institutos', cards, result);
        searchInField(data_arr, 'email', cards, result);
      })
      .catch(error => alert(error));

  if (result.length == 0){
    throw new Error("Pesquisa não encontrou resultados!");
  }
  if (result.length > 60){
    throw new Error("Pesquisa muito ampla, tente ser mais específico!");
  }

  return result;
}

export {submitForm};