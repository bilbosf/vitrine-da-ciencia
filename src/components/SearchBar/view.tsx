import * as React from "react";
import styles from "./searchbar.module.css";
import TextField from '@material-ui/core/TextField';
import InputAdornment from '@material-ui/core/InputAdornment';
import SearchIcon from '@material-ui/icons/Search';

const SearchBarView = (props: any) => {
  const {submit} = props;

  const [text, setText] = React.useState("");

  return (
    <div className={props.className}>
        <TextField 
            onChange={(event) => setText(event.target.value)}
            onKeyDown={(event) => { if(event.key == "Enter") submit(text); }}
            className={styles.textField} 
            id="search-bar"
            label="Buscar" 
            variant="outlined" 
            size="small" 
            InputProps={{endAdornment: (
                <InputAdornment position="end">
                    <SearchIcon onClick={(event) => submit(text)} style={{cursor: "pointer"}}/>
                </InputAdornment>
                ),}}/>
    </div>
  );

}

export default SearchBarView;