import * as React from "react";
import CardsController from "./controller";
import CardsView from "./view";

const CardsContainer = (props: any) => {
  return(
    <CardsController>
      <CardsView 
        cardsInfo={props.cardsInfo}
      />
    </CardsController>
  )
}

export default CardsContainer;