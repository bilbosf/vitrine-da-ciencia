import * as React from "react";
import FormValues from "../../types/FormValues";
import Card from "../Card";
import styles from "./cards.module.css";

interface ICardsView {
  cardsInfo: FormValues[]
}

const CardsView = (props: ICardsView) => {
  const {cardsInfo} = props;

  return(
    <div className={styles.wrapper} >
      {cardsInfo.map(item => (
          <Card 
            img="https://images.unsplash.com/photo-1603126857599-f6e157fa2fe6?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1950&q=80"
            title={item.titulo}
            description={item.descricao}
          /> 
        )
      )}
    </div>
  )
}

export default CardsView;