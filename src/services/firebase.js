// Import the functions you need from the SDKs you need
import * as firebase from "firebase";

import 'firebase/auth';

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyAxd0C9W5MYut0K2ZamqHfvEOPbtA49J9w",
  authDomain: "vitrine-da-ciencia.firebaseapp.com",
  projectId: "vitrine-da-ciencia",
  storageBucket: "vitrine-da-ciencia.appspot.com",
  messagingSenderId: "586750104848",
  appId: "1:586750104848:web:dbe865a73ca26817fb0195",
  measurementId: "G-F4JY861M5K"
};

// Initialize Firebase
firebase.initializeApp(firebaseConfig);

const auth = firebase.auth();

const signInWithEmailAndPassword = async (email, password) => {
  try {
    await auth.signInWithEmailAndPassword(email, password) 
    .then(user => {
      console.log(user.user.email);
    })
  } catch (err) {
    console.error(err);
    alert(err.message);
  }
};

export {
  auth,
  signInWithEmailAndPassword,
}

//export default firebase;
export default firebase;