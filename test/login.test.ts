import {expect} from 'chai';
import {login} from "../src/components/LoginModal/model";

describe("#login", () => {
  describe("CT1_login: when email is invalid", () => {
    it("should throw an error", async () => {
      try {
        let response = await login("abc@gmail","abcd1234")
      } catch (err) {
        expect(() => {throw err}).to.throw(Error,"Email inválido!");
      }
    })
  })

  describe("CT2_login: when email is unregistered", () => {
    it("should throw an error", async () => {
      try {
        let response = await login("pessoa_n_cadastrada@gmail.com","abcd1234")
      } catch (err) {
        expect(() => {throw err}).to.throw(Error,"O email não está cadastrado!");
      }
    })
  })

  describe("CT3_login: when password is incorrect", () => {
    it("should throw an error", async () => {
      try {
        let response = await login("pessoacadastrada@gmail.com","abcd1234")
      } catch (err) {
        expect(() => {throw err}).to.throw(Error,"Senha incorreta!");
      }
    })
  })

  describe("CT4_login: when email and password is correct", () => {
    it("should log in", async () => {
      try {
        let response = await login("pessoacadastrada@gmail.com","senha_Correta123")
        expect(response).to.equal("Efetuar login");
      } catch (err) {
        ;
      }
    })
  })
})