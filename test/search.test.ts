import chai, {expect} from 'chai';
import assertArrays from 'chai-arrays';
import {submitForm} from '../src/components/SearchBar/model'
chai.use(assertArrays);

const term = "";
const results = (term: any) => {
    if (term.length<2){
        let resultsArray= Array(61).fill(term);
        return resultsArray;
    }
    return []
}

const search = (term: any) => {
    term=term.trim();

    if (term==="" || term.length>=61){
        throw new Error('Pesquisa inválida!')
    }
    if (term.length<2){
        if (results(term).length>60){
            throw new Error('Pesquisa muito ampla, tente ser mais específico!')
        }
    }
    if (term==="Professor Cadastrado"){
        return ("Exibição de resultados da pesquisa.")
    }
    if (term==="Termo não presente no banco de dados"){
        throw new Error('Pesquisa não encontrou resultados!')
    }
}

describe("#search", () => {
  describe("CT1_search: When the search is invalid", () => {
    it("should throw an error", async () => {
      try {
        let response = await submitForm("")
      } catch (err) {
        expect(() => {throw err}).to.throw(Error,"Pesquisa inválida!");
      }
    })
  })

  describe("CT2_search: When the search returns more than 60 results", () => {
    it("should throw an error", async () => {
      try {
        let response = await submitForm("a");
      } catch (err) {
        expect(() => {throw err}).to.throw(Error,"Pesquisa muito ampla, tente ser mais específico!");
      }
    })
  })

  describe("CT3_search: When the search is successful", () => {
    it("should show the search results", async () => {
      try {
        let response = await submitForm("Professor Cadastrado");
        expect(response).to.be.array();
        expect(response.length === 0).to.be.false;
        expect(response.length > 60).to.be.false;
      } catch (err) {
        ;
      }
    })
  })

  describe("CT4_search: When the search can't find any correspondencies", () => {
    it("should throw an error", async () => {
      try {
        let response = await submitForm("XXXXXXXXXXXXXXXXXXXXXXXX");
      } catch (err) {
        expect(() => {throw err}).to.throw(Error,"Pesquisa não encontrou resultados!");
      }
    })
  })

  describe("CT5_search: When the search term is 61 characters long", () => {
    it("should throw an error", async () => {
      try {
        let response = await submitForm("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus erat dolor, bibendum eget purus s.");
      } catch (err) {
        expect(() => {throw err}).to.throw(Error,"Pesquisa inválida!");
      }
    })
  })

  describe("CT6_search: When the search term is more than 61 characters long", () => {
    it("should throw an error", async () => {
      try {
        let response = await submitForm("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean mauris nisi, ultricies eu egestas id, pellentesque sit amet arcu. Lorem ipsum gravida.");
      } catch (err) {
        expect(() => {throw err}).to.throw(Error,"Pesquisa inválida!");
      }
    })
  })


})