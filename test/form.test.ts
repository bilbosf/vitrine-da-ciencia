import {expect} from 'chai';
import {submitForm} from '../src/pages/Form/model';

const submitForm1 = (data: any) => {
  const tituloDaPesquisa = data.tituloDaPesquisa.trim();
  const instituicoesEnvolvidas = data.instituicoesEnvolvidas.trim();
  const descricao = data.descricao.trim();
  const comoEntrarEmContato = data.comoEntrarEmContato.trim();

  if (tituloDaPesquisa.length === 0 || tituloDaPesquisa.length > 100) {
    throw new Error('"Título de pesquisa" não preenchido ou não válido')
  }

  if (instituicoesEnvolvidas.length === 0 || instituicoesEnvolvidas.length > 500) {
    throw new Error('"Institutos envolvidos" não preenchido ou não válido')
  }

  if (descricao.length === 0 || descricao.length > 5000) {
    throw new Error('"Descrição da pesquisa" não preenchido ou não válido')
  }

  if (comoEntrarEmContato.length === 0 || comoEntrarEmContato.length > 300) {
    throw new Error('"Como interessados entrar em contato" não preenchido ou não válido')
  }

  return("Form enviado com sucesso!")
}

describe("#form", () => {
  describe('CT1_form: when "titulo da pesquisa" is invalid', () => {
    it("should throw an error", async () => {
      try {
        let response = await submitForm({
          titulo: "",
          institutos: "IC, IMECC, Eldorado",
          descricao: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis ex ex, ultricies non posuere id turpi.",
          contato: "Email: professorcadastrado@gmail.com ou telefone: (19)91234-5678"
        })
      } catch (err) {
        expect(() => {throw err}).to.throw(Error,'"Título de pesquisa" não preenchido ou não válido');
      }
    })
  })

  describe('CT2_form: when "instituicoes envolvidas" is invalid', () => {
    it("should throw an error", async () => {
      try {
        let response = await submitForm({
          titulo: "Sistemas distribuidos para área de ...",
          institutos: "",
          descricao: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis ex ex, ultricies non posuere id turpi.",
          contato: "Email: professorcadastrado@gmail.com ou telefone: (19)91234-5678"
        })
      } catch (err) {
        expect(() => {throw err}).to.throw(Error,'"Institutos envolvidos" não preenchido ou não válido');
      }
    })
  })

  describe('CT3_form: when "descricao da pesquisa" is invalid', () => {
    it("should throw an error", async () => {
      try {
        let response = await submitForm({
          titulo: "Sistemas distribuidos para área de ...",
          institutos: "IC, IMECC, Eldorado",
          descricao: "",
          contato: "Email: professorcadastrado@gmail.com ou telefone: (19)91234-5678"
        })
      } catch (err) {
        expect(() => {throw err}).to.throw(Error,'"Descrição da pesquisa" não preenchido ou não válido');
      }
    })
  })

  describe('CT4_form: when "como interessados entrar em contato" is invalid', () => {
    it("should throw an error", async () => {
      try {
        let response = await submitForm({
          titulo: "Sistemas distribuidos para área de ...",
          institutos: "IC, IMECC, Eldorado",
          descricao: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis ex ex, ultricies non posuere id turpi.",
          contato: ""
        })
      } catch (err) {
        expect(() => {throw err}).to.throw(Error,'"Como interessados entrar em contato" não preenchido ou não válido');
      }
    })
  })

  describe('CT5_form: when all data is valid', () => {
    it("should submit form successfully", async () => {
      try {
        let response = await submitForm({
          titulo: "Sistemas distribuidos para área de ...",
          institutos: "IC, IMECC, Eldorado",
          descricao: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis ex ex, ultricies non posuere id turpi.",
          contato: "Email: professorcadastrado@gmail.com ou telefone: (19)91234-5678"
        })
        expect(response).to.equal("Form enviado com sucesso!")
      } catch (err) {
        ;
      }
    })
  })
})