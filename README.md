# Vitrine da Ciência

Projeto voltado para divulgação científica feito para a disciplina de Engenharia de Software na Unicamp

## Integrantes
* Áureo Henrique e Silva Marques     - RA: 213374 
* Antonio Gabriel da Silva Fernandes - RA: 231551   
* Lethycia Maia de Souza             - RA: 256477
* Lindon Jonathan Sanley dos Santos Pereira Monroe  - RA: 220407
* Lucas de Paula Soares              - RA: 201867

## Arquitetura do projeto
### Diagramas C4
A arquitetura de componentes adotada pode ser vista nas figuras abaixo, que representam os 3 primeiros níveis do modelo C4:
#### C4 - Nível 1
![Nível 1 do modelo C4](images/c4nivel1.jpg)
#### C4 - Nível 2
![Nível 2 do modelo C4](images/c4nivel2.jpg)
#### C4 - Nível 3
![Nível 3 do modelo C4](images/c4nivel3.jpg)
 
### Estilo arquitetural
O estilo adotado foi o *Model-View-Controller* (MVC), já que esse estilo se presta muito bem a aplicações web. Esse modelo traz diversas vantagens, como a facilidade de manutenção, devido a uma separação explícita entre as diferentes funcionalidades dentro de cada componente.

![Modelo MVC](images/mvc.jpg)

Esse padrão se baseia na separação em três camadas:
* O `Model` representa o modelo de dados e serviços da aplicação;
* A `View` é a manifestação da aplicação com a qual o usuário interage diretamente, através da interface;
* O `Controller` é a camada responsável pela interação e sincronização entre o `Model` e a `View`, servindo como um mediador.

Dessa forma, cada um dos nossos componentes terá explicitamente separadas essas 3 camadas de funcionamento. Por exemplo, a estrutura de pastas do componente `Form` vai ser a seguinte:

~~~
├── form
│   ├── view
│   │   ├── index.tsx
│   │   └── styles.module.css
│   ├── controllers
│   │   ├── onInputChange.tsx
│   │   ├── onSubmit.tsx
│   │   └── ...
│   ├── models
│   │   ├── submitData.tsx
│   │   ├── validateInfo.tsx
│   │   └── ...
~~~

### Descrição dos componentes
* **Feed:** Exibe pesquisas que possam ser de interesse ao usuário, inicialmente mostrando as pesquisas em destaque na plataforma, mas pode ser atualizada se o usuário faz uma busca.
* **Mecanismo de busca:** Pode ser usado para buscar pesquisas por palavras-chave ou através de filtros, alterando assim o conteúdo do feed.
* **Card:** Componente dentro do feed que apresenta as informações importantes de uma pesquisa ou vaga.
* **Dashboard do professor:** Exibe ao professor, uma vez autenticado, as pesquisas que foram divulgadas por ele e o form para submissão de novas vagas, além de possibilitar que o professor disponibilize outras informações.
* **Form:** Usado pelo professor para submeter novas pesquisas à plataforma.
* **Login:** Componente responsável pela autenticação dos professores dentro da plataforma.

### Padrão de projeto
Será utilizado o padrão de projeto *Observer* em todos os componentes. Nesse padrão, o componente observa uma certa variável, de tal forma que mudanças na variável ocasionam alguma mudança no componente que a observa. O componente `Card`, por exemplo, observa um certo valor que define se o modal com detalhes da pesquisa é exibido ou não, e esse valor é controlado por um botão.

No entanto, devido ao escopo do projeto e ao fato de o software não ser orientado a objetos, a adoção de padrões de projeto complexos é bastante dificultada, já que a maior parte dos *design patterns* tem em mente esse paradigma de programação.

### Prototipação
Para guiar-nos no desenvolvimento das views, houve uma etapa de prototipação das Páginas principais do projeto utilizando a ferramenta Figma. 

Foram usados alguns componentes próximos aos da biblioteca do React "Material-UI" de forma a facilitar sua implementação.

![Página do Figma](images/figma.png)

Primeiramente foi redesenhada a página inicial, de forma a ser mais concreta com o objetivo do projeto.

![Página inicial](images/home.png)

Página do professor, onde os alunos encontrarão as informações dos professores, como sua área de pesquisa e vagas em projetos.

![Página do Professor](images/pagina-professor.png)

Login apenas para professores.

![Login](images/login.png)

Ao fazer o login, o professor poderá editar suas informações e suas pesquisas.

![Dashboard do Professor](images/dashboard-professor.png)


![Modal do Projeto](images/modal-projeto.png)

Na barra de busca da Home, ao pesquisar por professores, as informações apareceram desta forma:

![Busca por Professor](images/resultados-professor.png)